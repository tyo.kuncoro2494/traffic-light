#include <LiquidCrystal.h>
#include <DS3231.h>
#include <EEPROM.h>
#include <stdio.h>
#include <Wire.h>
#include <Keypad.h>
LiquidCrystal lcd (7, 6, 5, 4, 3, 2);
DS3231 rtc(SDA, SCL);



//RTC Tools
Time t;
int day;
int hor;
int min;
int sec;

// RTC var
String dayp;
int sh, sm, ss;
int day1;
const byte ROWS = 4; //four rows
const byte COLS = 4; //four columns
char keys[ROWS][COLS] = {
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'}
};
byte rowPins[ROWS] = {A0, A1, A2, A3}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {A4, A5, A6, A7}; //connect to the column pinouts of the keypad
Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

#define r1 23
#define y1 25
#define g1 27
#define r2 29
#define y2 31
#define g2 33
#define r3 35
#define y3 37
#define g3 39
#define r4 41
#define y4 43
#define g4 45
#define r5 22
#define y5 24
#define g5 26
#define r6 28
#define y6 30
#define g6 32
#define r7 34
#define y7 36
#define g7 38
#define r8 40
#define y8 42
#define g8 44
// lebels
//Schedule Variables
//int st11, st12, st13, st14, st15, st16, st17, st21, st22, st23, st24, st25, st26, st27, st31, st32, st33, st34, st35, st36, st37, st41, st42, st43, st44, st45, st46, st47;
String b = "";
int h11 = 5, h21 = 5, h31 = 5, h41 = 5;
int h12 = 8, h22 = 8, h32 = 8, h42 = 8;
int h13 = 11, h23 = 11, h33 = 11, h43 = 11;
int h14 = 14, h24 = 14, h34 = 14, h44 = 14;
int h15 = 17, h25 = 17, h35 = 17, h45 = 17;
int h16 = 20, h26 = 20, h36 = 20, h46 = 20;
int h17 = 23, h27 = 32, h37 = 23, h47 = 23;

int m11 = 10, m21 = 10, m31 = 10, m41 = 10;
int m12 = 15, m22 = 15, m32 = 15, m42 = 15;
int m13 = 20, m23 = 20, m33 = 20, m43 = 20;
int m14 = 25, m24 = 25, m34 = 25, m44 = 25;
int m15 = 30, m25 = 30, m35 = 30, m45 = 30;
int m16 = 35, m26 = 35, m36 = 35, m46 = 35;
int m17 = 40, m27 = 40, m37 = 40, m47 = 40;

// Time Set Variables
int t111 = 111, t112 = 112, t113 = 123, t114 = 114;
int t121 = 121, t122 = 122, t123 = 123, t124 = 124;
int t131 = 131, t132 = 132, t133 = 133, t134 = 134;
int t141 = 141, t142 = 142, t143 = 143, t144 = 144;
int t151 = 151, t152 = 152, t153 = 153, t154 = 154;
int t161 = 161, t162 = 162, t163 = 163, t164 = 164;

int t211 = 211, t212 = 212, t213 = 213, t214 = 214;
int t221 = 221, t222 = 222, t223 = 223, t224 = 224;
int t231 = 231, t232 = 232, t233 = 233, t234 = 234;
int t241 = 241, t242 = 242, t243 = 243, t244 = 244;
int t251 = 251, t252 = 252, t253 = 253, t254 = 254;
int t261 = 261, t262 = 262, t263 = 263, t264 = 264;

int t311 = 311, t312 = 312, t313 = 313, t314 = 314;
int t321 = 321, t322 = 322, t323 = 323, t324 = 324;
int t331 = 331, t332 = 332, t333 = 333, t334 = 334;
int t341 = 341, t342 = 342, t343 = 343, t344 = 344;
int t351 = 351, t352 = 352, t353 = 353, t354 = 354;
int t361 = 361, t362 = 362, t363 = 363, t364 = 364;

int t411 = 411, t412 = 412, t413 = 413, t414 = 414;
int t421 = 421, t422 = 422, t423 = 423, t424 = 424;
int t431 = 431, t432 = 432, t433 = 433, t434 = 434;
int t441 = 441, t442 = 442, t443 = 443, t444 = 444;
int t451 = 451, t452 = 452, t453 = 453, t454 = 454;
int t461 = 461, t462 = 462, t463 = 463, t464 = 464;

int t511 = 511, t512 = 512, t513 = 513, t514 = 514;
int t521 = 521, t522 = 522, t523 = 523, t524 = 524;
int t531 = 531, t532 = 532, t533 = 533, t534 = 534;
int t541 = 541, t542 = 542, t543 = 543, t544 = 544;
int t551 = 551, t552 = 552, t553 = 553, t554 = 554;
int t561 = 561, t562 = 562, t563 = 563, t564 = 564;

int t611 = 611, t612 = 612, t613 = 613, t614 = 614;
int t621 = 621, t622 = 622, t623 = 623, t624 = 624;
int t631 = 631, t632 = 632, t633 = 633, t634 = 634;
int t641 = 641, t642 = 642, t643 = 643, t644 = 644;
int t651 = 651, t652 = 652, t653 = 653, t654 = 654;
int t661 = 661, t662 = 662, t663 = 663, t664 = 664;
/*keterangan

   a = var AI yg di set
   s = ?
   p = ?
   q = var AI
*/

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  rtc.begin();
  lcd.begin(16, 2);
  rtc.setDOW(MONDAY);     // Set Day-of-Week to SUNDAY
  rtc.setTime(20, 59, 58);     // Set the time to 12:00:00 (24hr format)
  pinMode(r1, OUTPUT);
  pinMode(y1, OUTPUT);
  pinMode(g1, OUTPUT);
  pinMode(r2, OUTPUT);
  pinMode(y2, OUTPUT);
  pinMode(g2, OUTPUT);
  pinMode(r3, OUTPUT);
  pinMode(y3, OUTPUT);
  pinMode(g3, OUTPUT);
  pinMode(r4, OUTPUT);
  pinMode(y4, OUTPUT);
  pinMode(g4, OUTPUT);
  pinMode(r5, OUTPUT);
  pinMode(y5, OUTPUT);
  pinMode(g5, OUTPUT);
  pinMode(r6, OUTPUT);
  pinMode(y6, OUTPUT);
  pinMode(g6, OUTPUT);
  pinMode(r7, OUTPUT);
  pinMode(y7, OUTPUT);
  pinMode(g7, OUTPUT);
  pinMode(r8, OUTPUT);
  pinMode(y8, OUTPUT);
  pinMode(g8, OUTPUT);
  pinMode(53, INPUT);
  pinMode(47, INPUT);
  digitalWrite(53, HIGH);
  digitalWrite(47, HIGH);

  t111 = EEPROM.read(0);
  t112 = EEPROM.read(1);
  t113 = EEPROM.read(2);
  t114 = EEPROM.read(3);

  t121 = EEPROM.read(4);
  t122 = EEPROM.read(5);
  t123 = EEPROM.read(6);
  t124 = EEPROM.read(7);

  t131 = EEPROM.read(8);
  t132 = EEPROM.read(9);
  t133 = EEPROM.read(10);
  t134 = EEPROM.read(11);

  t141 = EEPROM.read(12);
  t142 = EEPROM.read(13);
  t143 = EEPROM.read(14);
  t144 = EEPROM.read(15);

  t151 = EEPROM.read(16);
  t152 = EEPROM.read(17);
  t153 = EEPROM.read(18);
  t154 = EEPROM.read(19);

  t161 = EEPROM.read(20);
  t162 = EEPROM.read(21);
  t163 = EEPROM.read(22);
  t164 = EEPROM.read(23);

  t211 = EEPROM.read(24);
  t212 = EEPROM.read(25);
  t213 = EEPROM.read(26);
  t214 = EEPROM.read(27);

  t221 = EEPROM.read(28);
  t222 = EEPROM.read(29);
  t223 = EEPROM.read(30);
  t224 = EEPROM.read(31);

  t231 = EEPROM.read(32);
  t232 = EEPROM.read(33);
  t233 = EEPROM.read(34);
  t234 = EEPROM.read(35);

  t241 = EEPROM.read(36);
  t242 = EEPROM.read(37);
  t243 = EEPROM.read(38);
  t244 = EEPROM.read(39);

  t251 = EEPROM.read(40);
  t252 = EEPROM.read(41);
  t253 = EEPROM.read(42);
  t254 = EEPROM.read(43);

  t261 = EEPROM.read(44);
  t262 = EEPROM.read(45);
  t263 = EEPROM.read(46);
  t264 = EEPROM.read(47);

  t311 = EEPROM.read(48);
  t312 = EEPROM.read(49);
  t313 = EEPROM.read(50);
  t314 = EEPROM.read(51);

  t321 = EEPROM.read(52);
  t322 = EEPROM.read(53);
  t323 = EEPROM.read(54);
  t324 = EEPROM.read(55);

  t331 = EEPROM.read(56);
  t332 = EEPROM.read(57);
  t333 = EEPROM.read(58);
  t334 = EEPROM.read(59);

  t341 = EEPROM.read(60);
  t342 = EEPROM.read(61);
  t343 = EEPROM.read(62);
  t344 = EEPROM.read(63);

  t351 = EEPROM.read(64);
  t352 = EEPROM.read(65);
  t353 = EEPROM.read(66);
  t354 = EEPROM.read(67);

  t361 = EEPROM.read(68);
  t362 = EEPROM.read(69);
  t363 = EEPROM.read(70);
  t364 = EEPROM.read(71);

  t411 = EEPROM.read(72);
  t412 = EEPROM.read(73);
  t413 = EEPROM.read(74);
  t414 = EEPROM.read(75);

  t421 = EEPROM.read(76);
  t422 = EEPROM.read(77);
  t423 = EEPROM.read(78);
  t424 = EEPROM.read(79);

  t431 = EEPROM.read(80);
  t432 = EEPROM.read(81);
  t433 = EEPROM.read(82);
  t434 = EEPROM.read(83);

  t441 = EEPROM.read(84);
  t442 = EEPROM.read(85);
  t443 = EEPROM.read(86);
  t444 = EEPROM.read(87);

  t451 = EEPROM.read(88);
  t452 = EEPROM.read(89);
  t453 = EEPROM.read(90);
  t454 = EEPROM.read(91);

  t461 = EEPROM.read(92);
  t462 = EEPROM.read(93);
  t463 = EEPROM.read(94);
  t464 = EEPROM.read(95);

  t511 = EEPROM.read(96);
  t512 = EEPROM.read(97);
  t513 = EEPROM.read(98);
  t514 = EEPROM.read(99);

  t521 = EEPROM.read(100);
  t522 = EEPROM.read(101);
  t523 = EEPROM.read(102);
  t524 = EEPROM.read(103);

  t531 = EEPROM.read(104);
  t532 = EEPROM.read(105);
  t533 = EEPROM.read(106);
  t534 = EEPROM.read(107);

  t541 = EEPROM.read(108);
  t542 = EEPROM.read(109);
  t543 = EEPROM.read(110);
  t544 = EEPROM.read(111);

  t551 = EEPROM.read(112);
  t552 = EEPROM.read(113);
  t553 = EEPROM.read(114);
  t554 = EEPROM.read(115);

  t561 = EEPROM.read(116);
  t562 = EEPROM.read(117);
  t563 = EEPROM.read(118);
  t564 = EEPROM.read(119);

  t611 = EEPROM.read(120);
  t612 = EEPROM.read(121);
  t613 = EEPROM.read(122);
  t614 = EEPROM.read(123);

  t621 = EEPROM.read(124);
  t622 = EEPROM.read(125);
  t623 = EEPROM.read(126);
  t624 = EEPROM.read(127);

  t631 = EEPROM.read(128);
  t632 = EEPROM.read(129);
  t633 = EEPROM.read(130);
  t634 = EEPROM.read(131);

  t641 = EEPROM.read(132);
  t642 = EEPROM.read(133);
  t643 = EEPROM.read(134);
  t644 = EEPROM.read(135);

  t651 = EEPROM.read(136);
  t652 = EEPROM.read(137);
  t653 = EEPROM.read(138);
  t654 = EEPROM.read(139);

  t661 = EEPROM.read(140);
  t662 = EEPROM.read(141);
  t663 = EEPROM.read(142);
  t664 = EEPROM.read(143);


}
//Global Variables
String jds = "";
int jd;
byte cr;

byte p = 0;
byte s = 0;
byte a = 0;
int q = 0;

int os = 0;

void loop() {
  char key = keypad.getKey();
  t = rtc.getTime();
  day = t.dow;
  hor = t.hour;
  min = t.min;
  sec = t.sec;

  Serial.print(p);
  Serial.print(" ");
  Serial.print(s);
  Serial.print(" ");
  Serial.print(a);
  Serial.print(" ");
  Serial.print(os);
  Serial.print(" ");
  Serial.println(q);
  /*  //testing
      int z = 0;
      z = min;
      z++;
      rtc.setTime(hor, z , min);
      delay(10);
    }
    if (digitalRead(47) == LOW) {
      rtc.setTime(hor, min, 59);
    }
    lcd.setCursor(7, 1);
    lcd.print(day);
    //endTesting*/
  if (os == 0 ) {
    lcd.setCursor(8, 0);
    lcd.print(rtc.getTimeStr());
    //    lcd.setCursor(0, 1);
    //    lcd.print(a);
    Sync();
    if (key == 'A') {
      lcd.clear();
      cr = 0;
      lcd.setCursor(0, 0);
      lcd.print("SETTING JAM");
      lcd.setCursor(0, 1);
      lcd.print(hor);
      lcd.print(":");
      lcd.print(min);
      lcd.print(":00  ");
      lcd.print(dayp);
      os = 1;
    } else if (key == 'B') {
      lcd.clear();
      schedule();
      key = 0;
      os = 2;
    } else if (key == 'C') {
      os = 3;
    } else if (key == 'D') {
      os = 4;
    }
    key = 0;
  }
  if (os == 1) {
    //seta
    if (key != NO_KEY && (key == '*' || key == '1' || key == '2' || key == '3' || key == '4' || key == '5' || key == '6' || key == '7' || key == '8' || key == '9' || key == '0')) {
      lcd.setCursor(cr, 1);
      lcd.print(key);
      jds = jds + key;
      cr ++;
      if (cr == 2) {
        sh = jds.toInt();
        jds = "";
        cr = 3;
      }
      if (cr == 5) {
        sm = jds.toInt();
        jds = "";
        cr = 6;
      }
      if (cr == 8) {
        ss = jds.toInt();
        rtc.setTime(sh, sm, ss);
        jds = "";
        cr = 0;
        os = 11;
      }
      if (key == '*') {
        jds = "";
        cr = 0;
        os = 0;
      }
    }
  }
  if (os == 11) {
    if (key == '0') {
      day1 = 1;
      lcd.setCursor(10, 1);
      lcd.print("SENIN ");
    }
    if (key == '1') {
      day1 = 2;
      lcd.setCursor(10, 1);
      lcd.print("SELASA");
    }
    if (key == '2') {
      day1 = 3;
      lcd.setCursor(10, 1);
      lcd.print("RABU  ");
    }
    if (key == '3') {
      day1 = 4;
      lcd.setCursor(10, 1);
      lcd.print("KAMIS ");
    }
    if (key == '4') {
      day1 = 5;
      lcd.setCursor(10, 1);
      lcd.print("JUM'AT");
    }
    if (key == '5') {
      day1 = 6;
      lcd.setCursor(10, 1);
      lcd.print("SABTU ");
    }
    if (key == '6') {
      day1 = 7;
      lcd.setCursor(10, 1);
      lcd.print("MINGGU");
    }
    if (key == '*') {
      lcd.clear();
      os = 0;
    }

    if (key == '#') {
      rtc.setDOW(day1);
      day1 = 9;
      lcd.clear();
      os = 0;
    }

  }


  if (os == 2) {
    //setb
    if (q > 0 && q < 10) {
      lcd.setCursor(0, 0); lcd.print("SET PLAN        ");
      lcd.setCursor(0, 1); lcd.print("1,2,3,4,5,6,7");
    } if (q > 10 && q < 100) {
      lcd.setCursor(0, 0); lcd.print("SET STAGE       ");
    } if (q > 100 && q < 1000) {
      set();
      os = 22;
    }
    if (key == '1') {
      a = 1;
      if (q > 0 && q < 10) {
        p = 1;
        a = 10;
      } if (q > 10) {
        s = 1;
        a = 100;
      }
      q += a;
      delay(50);
    } else if (key == '2') {
      a = 2;
      if (q > 0 && q < 10) {
        p = 2;
        a = 20;
      } if (q > 10) {
        s = 2;
        a = 200;
      }
      q += a;
      delay(50);
    } else if (key == '3') {
      a = 3;
      if (q > 0 && q < 10) {
        p = 3;
        a = 30;
      } if (q > 10) {
        s = 3;
        a = 300;
      }
      q += a;
      delay(50);
    } else if (key == '4') {
      a = 4;
      if (q > 0 && q < 10) {
        p = 4;
        a = 40;
      } if (q > 10) {
        s = 4;
        a = 400;
      }
      q += a;
      delay(50);
    } else if (key == '5') {
      a = 0;
      if (q > 0 && q < 10) {
        p = 5;
        a = 50;
      } if (q > 10) {
        s = 5;
        a = 500;
      }
      q += a;
      delay(50);
    } else if (key == '6') {
      a = 0;
      if (q > 0 && q < 10) {
        p = 6;
        a = 60;
      } if (q > 10) {
        s = 6;
        a = 600;
      }
      q += a;
      delay(50);
    } else if (key == '*') {
      q = 0; p = 0; s = 0; a = 0; lcd.clear(); os = 0;
    }
    key = 0;
  }
  if (os == 22) {
    if (key != NO_KEY && (key == '*' || key == '1' || key == '2' || key == '3' || key == '4' || key == '5' || key == '6' || key == '7' || key == '8' || key == '9' || key == '0')) {
      if (q != 0) {
        cr ++;
      }
      lcd.setCursor(cr, 1);
      lcd.print(key);
      jds += key;
      if (key == '*') {
        cr--;
        cr = 11;
        jds = "";
        key = 0;
        q = 0; p = 0; s = 0; a = 0;
        os = 0;
        lcd.clear();
      }
      if (cr == 13) {
        cr = 17;
        key = 0;
        q = 0; p = 0; s = 0; a = 0;
        switch (q) {
          case 111: t111 = jd; jd = 0; EEPROM.write(0, t111); break;
          case 112: t112 = jd; jd = 0; EEPROM.write(1, t112); break;
          case 113: t113 = jd; jd = 0; EEPROM.write(2, t113); break;
          case 114: t114 = jd; jd = 0; EEPROM.write(3, t114); break;

          case 121: t121 = jd; jd = 0; EEPROM.write(4, t121); break;
          case 122: t122 = jd; jd = 0; EEPROM.write(5, t122); break;
          case 123: t123 = jd; jd = 0; EEPROM.write(6, t123); break;
          case 124: t124 = jd; jd = 0; EEPROM.write(7, t124); break;

          case 131: t131 = jd; jd = 0; EEPROM.write(8, t131); break;
          case 132: t132 = jd; jd = 0; EEPROM.write(9, t132); break;
          case 133: t133 = jd; jd = 0; EEPROM.write(10, t133); break;
          case 134: t134 = jd; jd = 0; EEPROM.write(11, t134); break;

          case 141: t141 = jd; jd = 0; EEPROM.write(12, t141); break;
          case 142: t142 = jd; jd = 0; EEPROM.write(13, t142); break;
          case 143: t143 = jd; jd = 0; EEPROM.write(14, t143); break;
          case 144: t144 = jd; jd = 0; EEPROM.write(15, t144); break;

          case 151: t151 = jd; jd = 0; EEPROM.write(16, t151); break;
          case 152: t152 = jd; jd = 0; EEPROM.write(17, t152); break;
          case 153: t153 = jd; jd = 0; EEPROM.write(18, t153); break;
          case 154: t154 = jd; jd = 0; EEPROM.write(19, t154); break;

          case 161: t161 = jd; jd = 0; EEPROM.write(20, t161); break;
          case 162: t162 = jd; jd = 0; EEPROM.write(21, t162); break;
          case 163: t163 = jd; jd = 0; EEPROM.write(22, t163); break;
          case 164: t164 = jd; jd = 0; EEPROM.write(23, t164); break;


          case 211: t211 = jd; jd = 0; EEPROM.write(24, t211); break;
          case 212: t212 = jd; jd = 0; EEPROM.write(25, t212); break;
          case 213: t213 = jd; jd = 0; EEPROM.write(26, t213); break;
          case 214: t214 = jd; jd = 0; EEPROM.write(27, t214); break;

          case 221: t221 = jd; jd = 0; EEPROM.write(28, t221); break;
          case 222: t222 = jd; jd = 0; EEPROM.write(29, t222); break;
          case 223: t223 = jd; jd = 0; EEPROM.write(30, t223); break;
          case 224: t224 = jd; jd = 0; EEPROM.write(31, t224); break;

          case 231: t231 = jd; jd = 0; EEPROM.write(32, t231); break;
          case 232: t232 = jd; jd = 0; EEPROM.write(33, t232); break;
          case 233: t233 = jd; jd = 0; EEPROM.write(34, t233); break;
          case 234: t234 = jd; jd = 0; EEPROM.write(35, t234); break;

          case 241: t241 = jd; jd = 0; EEPROM.write(36, t241); break;
          case 242: t242 = jd; jd = 0; EEPROM.write(37, t242); break;
          case 243: t243 = jd; jd = 0; EEPROM.write(38, t243); break;
          case 244: t244 = jd; jd = 0; EEPROM.write(39, t244); break;

          case 251: t251 = jd; jd = 0; EEPROM.write(40, t251); break;
          case 252: t252 = jd; jd = 0; EEPROM.write(41, t252); break;
          case 253: t253 = jd; jd = 0; EEPROM.write(42, t253); break;
          case 254: t254 = jd; jd = 0; EEPROM.write(43, t254); break;

          case 261: t261 = jd; jd = 0; EEPROM.write(44, t261); break;
          case 262: t262 = jd; jd = 0; EEPROM.write(45, t262); break;
          case 263: t263 = jd; jd = 0; EEPROM.write(46, t263); break;
          case 264: t264 = jd; jd = 0; EEPROM.write(47, t264); break;



          case 311: t311 = jd; jd = 0; EEPROM.write(48, t311); break;
          case 312: t312 = jd; jd = 0; EEPROM.write(49, t312); break;
          case 313: t313 = jd; jd = 0; EEPROM.write(50, t313); break;
          case 314: t314 = jd; jd = 0; EEPROM.write(51, t314); break;

          case 321: t321 = jd; jd = 0; EEPROM.write(52, t321); break;
          case 322: t322 = jd; jd = 0; EEPROM.write(53, t322); break;
          case 323: t323 = jd; jd = 0; EEPROM.write(54, t323); break;
          case 324: t324 = jd; jd = 0; EEPROM.write(55, t324); break;

          case 331: t331 = jd; jd = 0; EEPROM.write(56, t331); break;
          case 332: t332 = jd; jd = 0; EEPROM.write(57, t332); break;
          case 333: t333 = jd; jd = 0; EEPROM.write(58, t333); break;
          case 334: t334 = jd; jd = 0; EEPROM.write(59, t334); break;

          case 341: t341 = jd; jd = 0; EEPROM.write(60, t341); break;
          case 342: t342 = jd; jd = 0; EEPROM.write(61, t342); break;
          case 343: t343 = jd; jd = 0; EEPROM.write(62, t343); break;
          case 344: t344 = jd; jd = 0; EEPROM.write(63, t344); break;

          case 351: t351 = jd; jd = 0; EEPROM.write(64, t351); break;
          case 352: t352 = jd; jd = 0; EEPROM.write(65, t352); break;
          case 353: t353 = jd; jd = 0; EEPROM.write(66, t353); break;
          case 354: t354 = jd; jd = 0; EEPROM.write(67, t354); break;

          case 361: t361 = jd; jd = 0; EEPROM.write(68, t361); break;
          case 362: t362 = jd; jd = 0; EEPROM.write(69, t362); break;
          case 363: t363 = jd; jd = 0; EEPROM.write(70, t363); break;
          case 364: t364 = jd; jd = 0; EEPROM.write(71, t364); break;


          case 411: t411 = jd; jd = 0; EEPROM.write(72, t411); break;
          case 412: t412 = jd; jd = 0; EEPROM.write(73, t412); break;
          case 413: t413 = jd; jd = 0; EEPROM.write(74, t413); break;
          case 414: t414 = jd; jd = 0; EEPROM.write(75, t414); break;

          case 421: t421 = jd; jd = 0; EEPROM.write(76, t421); break;
          case 422: t422 = jd; jd = 0; EEPROM.write(77, t422); break;
          case 423: t423 = jd; jd = 0; EEPROM.write(78, t423); break;
          case 424: t424 = jd; jd = 0; EEPROM.write(79, t424); break;

          case 431: t431 = jd; jd = 0; EEPROM.write(80, t431); break;
          case 432: t432 = jd; jd = 0; EEPROM.write(81, t432); break;
          case 433: t433 = jd; jd = 0; EEPROM.write(82, t433); break;
          case 434: t434 = jd; jd = 0; EEPROM.write(83, t434); break;

          case 441: t441 = jd; jd = 0; EEPROM.write(84, t441); break;
          case 442: t442 = jd; jd = 0; EEPROM.write(85, t442); break;
          case 443: t443 = jd; jd = 0; EEPROM.write(86, t443); break;
          case 444: t444 = jd; jd = 0; EEPROM.write(87, t444); break;

          case 451: t451 = jd; jd = 0; EEPROM.write(88, t451); break;
          case 452: t452 = jd; jd = 0; EEPROM.write(89, t452); break;
          case 453: t453 = jd; jd = 0; EEPROM.write(90, t453); break;
          case 454: t454 = jd; jd = 0; EEPROM.write(91, t454); break;

          case 461: t461 = jd; jd = 0; EEPROM.write(92, t461); break;
          case 462: t462 = jd; jd = 0; EEPROM.write(93, t462); break;
          case 463: t463 = jd; jd = 0; EEPROM.write(94, t463); break;
          case 464: t464 = jd; jd = 0; EEPROM.write(95, t464); break;


          case 511: t511 = jd; jd = 0; EEPROM.write(96, t511); break;
          case 512: t512 = jd; jd = 0; EEPROM.write(97, t512); break;
          case 513: t513 = jd; jd = 0; EEPROM.write(98, t513); break;
          case 514: t514 = jd; jd = 0; EEPROM.write(99, t514); break;

          case 521: t521 = jd; jd = 0; EEPROM.write(100, t521); break;
          case 522: t522 = jd; jd = 0; EEPROM.write(101, t522); break;
          case 523: t523 = jd; jd = 0; EEPROM.write(102, t523); break;
          case 524: t524 = jd; jd = 0; EEPROM.write(103, t524); break;

          case 531: t531 = jd; jd = 0; EEPROM.write(104, t531); break;
          case 532: t532 = jd; jd = 0; EEPROM.write(105, t532); break;
          case 533: t533 = jd; jd = 0; EEPROM.write(106, t533); break;
          case 534: t534 = jd; jd = 0; EEPROM.write(107, t534); break;

          case 541: t541 = jd; jd = 0; EEPROM.write(108, t541); break;
          case 542: t542 = jd; jd = 0; EEPROM.write(109, t542); break;
          case 543: t543 = jd; jd = 0; EEPROM.write(110, t543); break;
          case 544: t544 = jd; jd = 0; EEPROM.write(111, t544); break;

          case 551: t551 = jd; jd = 0; EEPROM.write(112, t551); break;
          case 552: t552 = jd; jd = 0; EEPROM.write(113, t552); break;
          case 553: t553 = jd; jd = 0; EEPROM.write(114, t553); break;
          case 554: t554 = jd; jd = 0; EEPROM.write(115, t554); break;

          case 561: t561 = jd; jd = 0; EEPROM.write(116, t561); break;
          case 562: t562 = jd; jd = 0; EEPROM.write(117, t562); break;
          case 563: t563 = jd; jd = 0; EEPROM.write(118, t563); break;
          case 564: t564 = jd; jd = 0; EEPROM.write(119, t564); break;


          case 611: t611 = jd; jd = 0; EEPROM.write(120, t611); break;
          case 612: t612 = jd; jd = 0; EEPROM.write(121, t612); break;
          case 613: t613 = jd; jd = 0; EEPROM.write(122, t613); break;
          case 614: t614 = jd; jd = 0; EEPROM.write(123, t614); break;

          case 621: t621 = jd; jd = 0; EEPROM.write(124, t621); break;
          case 622: t622 = jd; jd = 0; EEPROM.write(125, t622); break;
          case 623: t623 = jd; jd = 0; EEPROM.write(126, t623); break;
          case 624: t624 = jd; jd = 0; EEPROM.write(127, t624); break;

          case 631: t631 = jd; jd = 0; EEPROM.write(128, t631); break;
          case 632: t632 = jd; jd = 0; EEPROM.write(129, t632); break;
          case 633: t633 = jd; jd = 0; EEPROM.write(130, t633); break;
          case 634: t634 = jd; jd = 0; EEPROM.write(131, t634); break;

          case 641: t641 = jd; jd = 0; EEPROM.write(132, t641); break;
          case 642: t642 = jd; jd = 0; EEPROM.write(133, t642); break;
          case 643: t643 = jd; jd = 0; EEPROM.write(134, t643); break;
          case 644: t644 = jd; jd = 0; EEPROM.write(135, t644); break;

          case 651: t651 = jd; jd = 0; EEPROM.write(136, t651); break;
          case 652: t652 = jd; jd = 0; EEPROM.write(137, t652); break;
          case 653: t653 = jd; jd = 0; EEPROM.write(138, t653); break;
          case 654: t654 = jd; jd = 0; EEPROM.write(139, t654); break;

          case 661: t661 = jd; jd = 0; EEPROM.write(140, t661); break;
          case 662: t662 = jd; jd = 0; EEPROM.write(141, t662); break;
          case 663: t663 = jd; jd = 0; EEPROM.write(142, t663); break;
          case 664: t664 = jd; jd = 0; EEPROM.write(143, t664); break;
        }
        lcd.setCursor(15, 1);
        lcd.print("*");
        if (key == '*' ) {
          os = 0;
          lcd.clear();
        }
      }
    }
  }
  if (os == 3) {
    //setc
    lcd.setCursor(0, 0);
    lcd.print("SET SCHEDULE");
    if (key == '*') {
      lcd.clear();
      os = 0;
    }
  }
  if (os == 4) {
    //setd
    lcd.setCursor(0, 0);
    lcd.print("setD");
    if (key == '*') {
      lcd.clear();
      os = 0;
    }
  }
  // put your main code here, to run repeatedly:

}

void Sync() {
  lcd.setCursor(0, 0);
  lcd.print(dayp);
  if (day == 0) {
    rtc.setDOW(MONDAY);
  }
  switch (day) {
    case 1:  dayp = "SENIN "; break;
    case 2:  dayp = "SELASA"; break;
    case 3:  dayp = "RABU  "; break;
    case 4:  dayp = "KAMIS "; break;
    case 5:  dayp = "JUMAT "; break;
    case 6:  dayp = "SABTU "; break;
    case 7:  dayp = "MINGGU"; break;
  }
  //d1-4
  if (day == 1 || day == 2 || day == 3 || day == 4) {
    if (hor >= h11 && hor <= h12) {
      if (min >= m11) {
        b = "st11";
      }
      if (hor == h12 && min >= m12) {
        b = "st12";
      }
    }

    if (hor >= h13 && hor <= h14) {
      if (min >= m13) {
        b = "st13";
      }
      if (hor == h14 && min >= m14) {
        b = "st14";
      }
    }

    if (hor >= h15 && hor <= h16) {
      if (min >= m15) {
        b = "st15";
      }
      if (hor == h16 && min >=  m16) {
        b = "st16";
      }
    }

    if (hor >= h17 && min >= m17) {
      b = "st17";
    }

    if (hor <= h11 && min < m11) {
      if (day == 1) {
        b = "st47";
      }
      if (day >= 2  && day <= 4) {
        b = "st17";
      }
    }
  }
  //d5
  if (day == 5) {
    if (hor >= h21 && hor <= h22) {
      if (min >= m21) {
        b = "st21";
      }
      if (hor == h22 && min >= m22) {
        b = "st22";
      }
    }

    if (hor >= h23 && hor <= h24) {
      if (min >= m23) {
        b = "st23";
      }
      if (hor == h24 && min >= m24) {
        b = "st24";
      }
    }

    if (hor >= h25 && hor <= h26) {
      if (min >= m25) {
        b = "st25";
      }
      if (hor == h26 && min >=  m26) {
        b = "st26";
      }
    }

    if (hor >= h27 && min >= m27) {
      b = "st27";
    }

    if (hor <= h21 && min < m21) {
      b = "st17";
    }
  }
  //d6
  if (day == 6) {
    if (hor >= h31 && hor <= h32) {
      if (min >= m31) {
        b = "st31";
      }
      if (hor == h32 && min >= m32) {
        b = "st32";
      }
    }

    if (hor >= h33 && hor <= h34) {
      if (min >= m33) {
        b = "st33";
      }
      if (hor == h34 && min >= m34) {
        b = "st34";
      }
    }

    if (hor >= h35 && hor <= h36) {
      if (min >= m35) {
        b = "st35";
      }
      if (hor == h36 && min >=  m36) {
        b = "st36";
      }
    }

    if (hor >= h37 && min >= m37) {
      b = "st37";
    }

    if (hor <= h31 && min < m31) {
      b = "st27";
    }
  }
  //d7
  if (day == 7) {
    if (hor >= h41 && hor <= h42) {
      if (min >= m41) {
        b = "st41";
      }
      if (hor == h42 && min >= m42) {
        b = "st42";
      }
    }

    if (hor >= h43 && hor <= h44) {
      if (min >= m43) {
        b = "st43";
      }
      if (hor == h44 && min >= m44) {
        b = "st44";
      }
    }

    if (hor >= h45 && hor <= h46) {
      if (min >= m45) {
        b = "st45";
      }
      if (hor == h46 && min >=  m46) {
        b = "st46";
      }
    }

    if (hor >= h47 && min >= m47) {
      b = "st47";
    }

    if (hor <= h41 && min < m41) {
      b = "st37";
    }
  }
}
void schedule() {
  lcd.setCursor(0, 0); lcd.print("SET SCHEDULE");
  lcd.setCursor(0, 1); lcd.print("1,2,3,4,5,6,7");
}
void plan () {
  lcd.setCursor(0, 0); lcd.print("SET PLAN");
  lcd.setCursor(0, 1); lcd.print("1,2,3,4,5,6,7");
}
void stage () {
  lcd.setCursor(0, 0);
  lcd.print("STAGE 123456,7");
}
void set() {
  lcd.setCursor(0, 0);
  lcd.print("SET    P  S     ");
  lcd.setCursor(8, 0);
  lcd.print(p);
  lcd.setCursor(11, 0);
  lcd.print(s);
  lcd.setCursor(0, 1);
  lcd.print("Hijau    ->     ");
  lcd.setCursor(6, 1);
  switch (q) {
    case 111: lcd.print(t111); break;
    case 112: lcd.print(t112); break;
    case 113: lcd.print(t113); break;
    case 114: lcd.print(t114); break;

    case 121: lcd.print(t121); break;
    case 122: lcd.print(t122); break;
    case 123: lcd.print(t123); break;
    case 124: lcd.print(t124); break;

    case 131: lcd.print(t131); break;
    case 132: lcd.print(t132); break;
    case 133: lcd.print(t133); break;
    case 134: lcd.print(t134); break;

    case 141: lcd.print(t141); break;
    case 142: lcd.print(t142); break;
    case 143: lcd.print(t143); break;
    case 144: lcd.print(t144); break;

    case 151: lcd.print(t151); break;
    case 152: lcd.print(t152); break;
    case 153: lcd.print(t153); break;
    case 154: lcd.print(t154); break;

    case 161: lcd.print(t161); break;
    case 162: lcd.print(t162); break;
    case 163: lcd.print(t163); break;
    case 164: lcd.print(t164); break;

    case 211: lcd.print(t211); break;
    case 212: lcd.print(t212); break;
    case 213: lcd.print(t213); break;
    case 214: lcd.print(t214); break;

    case 221: lcd.print(t221); break;
    case 222: lcd.print(t222); break;
    case 223: lcd.print(t223); break;
    case 224: lcd.print(t224); break;

    case 231: lcd.print(t231); break;
    case 232: lcd.print(t232); break;
    case 233: lcd.print(t233); break;
    case 234: lcd.print(t234); break;

    case 241: lcd.print(t241); break;
    case 242: lcd.print(t242); break;
    case 243: lcd.print(t243); break;
    case 244: lcd.print(t244); break;

    case 251: lcd.print(t251); break;
    case 252: lcd.print(t252); break;
    case 253: lcd.print(t253); break;
    case 254: lcd.print(t254); break;

    case 261: lcd.print(t261); break;
    case 262: lcd.print(t262); break;
    case 263: lcd.print(t263); break;
    case 264: lcd.print(t264); break;

    case 311: lcd.print(t311); break;
    case 312: lcd.print(t312); break;
    case 313: lcd.print(t313); break;
    case 314: lcd.print(t314); break;

    case 321: lcd.print(t321); break;
    case 322: lcd.print(t322); break;
    case 323: lcd.print(t323); break;
    case 324: lcd.print(t324); break;

    case 331: lcd.print(t331); break;
    case 332: lcd.print(t332); break;
    case 333: lcd.print(t333); break;
    case 334: lcd.print(t334); break;

    case 341: lcd.print(t341); break;
    case 342: lcd.print(t342); break;
    case 343: lcd.print(t343); break;
    case 344: lcd.print(t344); break;

    case 351: lcd.print(t351); break;
    case 352: lcd.print(t352); break;
    case 353: lcd.print(t353); break;
    case 354: lcd.print(t354); break;

    case 361: lcd.print(t361); break;
    case 362: lcd.print(t362); break;
    case 363: lcd.print(t363); break;
    case 364: lcd.print(t364); break;

    case 411: lcd.print(t411); break;
    case 412: lcd.print(t412); break;
    case 413: lcd.print(t413); break;
    case 414: lcd.print(t414); break;

    case 421: lcd.print(t421); break;
    case 422: lcd.print(t422); break;
    case 423: lcd.print(t423); break;
    case 424: lcd.print(t424); break;

    case 431: lcd.print(t431); break;
    case 432: lcd.print(t432); break;
    case 433: lcd.print(t433); break;
    case 434: lcd.print(t434); break;

    case 441: lcd.print(t441); break;
    case 442: lcd.print(t442); break;
    case 443: lcd.print(t443); break;
    case 444: lcd.print(t444); break;

    case 451: lcd.print(t451); break;
    case 452: lcd.print(t452); break;
    case 453: lcd.print(t453); break;
    case 454: lcd.print(t454); break;

    case 461: lcd.print(t461); break;
    case 462: lcd.print(t462); break;
    case 463: lcd.print(t463); break;
    case 464: lcd.print(t464); break;

    case 511: lcd.print(t511); break;
    case 512: lcd.print(t512); break;
    case 513: lcd.print(t513); break;
    case 514: lcd.print(t514); break;

    case 521: lcd.print(t521); break;
    case 522: lcd.print(t522); break;
    case 523: lcd.print(t523); break;
    case 524: lcd.print(t524); break;

    case 531: lcd.print(t531); break;
    case 532: lcd.print(t532); break;
    case 533: lcd.print(t533); break;
    case 534: lcd.print(t534); break;

    case 541: lcd.print(t541); break;
    case 542: lcd.print(t542); break;
    case 543: lcd.print(t543); break;
    case 544: lcd.print(t544); break;

    case 551: lcd.print(t551); break;
    case 552: lcd.print(t552); break;
    case 553: lcd.print(t553); break;
    case 554: lcd.print(t554); break;

    case 561: lcd.print(t561); break;
    case 562: lcd.print(t562); break;
    case 563: lcd.print(t563); break;
    case 564: lcd.print(t564); break;

    case 611: lcd.print(t611); break;
    case 612: lcd.print(t612); break;
    case 613: lcd.print(t613); break;
    case 614: lcd.print(t614); break;

    case 621: lcd.print(t621); break;
    case 622: lcd.print(t622); break;
    case 623: lcd.print(t623); break;
    case 624: lcd.print(t624); break;

    case 631: lcd.print(t631); break;
    case 632: lcd.print(t632); break;
    case 633: lcd.print(t633); break;
    case 634: lcd.print(t634); break;

    case 641: lcd.print(t641); break;
    case 642: lcd.print(t642); break;
    case 643: lcd.print(t643); break;
    case 644: lcd.print(t644); break;

    case 651: lcd.print(t651); break;
    case 652: lcd.print(t652); break;
    case 653: lcd.print(t653); break;
    case 654: lcd.print(t654); break;

    case 661: lcd.print(t661); break;
    case 662: lcd.print(t662); break;
    case 663: lcd.print(t663); break;
    case 664: lcd.print(t664); break;
  }
}
